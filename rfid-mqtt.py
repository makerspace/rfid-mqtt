#!/usr/bin/python3

# rfid-mqtt
# RedBee RFID Reader to MQTT Message Adapter
#
# Example Token string:
# T:NACK 239 0 0 0 254\n
#
# CURRIE June 2017

import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
# import json
import sys  #for exit
import os # for pid
import struct
import time
import socket
import argparse

# crude simple order-based commandline arguments
# scripty.py [name] [remote_ip] [remote_port]
if (len(sys.argv) >= 4):
    READER_NAME = sys.argv[1]
    REMOTE_ADDR = sys.argv[2]
    REMOTE_PORT = int(sys.argv[3])
else:
    print("missing commandline arguments..")
    print("Usage rfid-mqtt.py <reader_name> <remote_ip> <remote_port>")
    sys.exit()

# T:NACK 239 0 0 0 254\n

TOKEN_TEST = "c003133d6e"
PID_FILE = "/var/run/rfid-mqtt.pid"

# REMOTE TCP DATA
TCP_BUFFER_SIZE = 4096
LINE_DELIMITER = "\n"

# MQTT BROKER
MQTT_HOST = "localhost"
MQTT_PORT = 1883
MQTT_TOPIC = "idac/zone/makerspace/event/" + READER_NAME

#idac/zone/makerspace/event/entry and 
#idac/zone/makerspace/event/exit

# Write PID to PID file
if (len(sys.argv) == 5):
    PID_FILE = sys.argv[4]
    debugFile = "/etc/monit/pid/debug.log"
    f = open(PID_FILE, 'w')
    f.write(str(os.getpid()))
    f.close
    
    f = open(debugFile, 'w')
    f.write("hello")
    f.write("Parent Process ID " + str(os.getpid()))
    f.close
    
    

#idac#  

def processReceivedLine(line):
    # ensure this is a likely token string
    if ("ACK " in line.decode('utf-8')):
        bytes = line[line.index(str("ACK ").encode('utf-8'))+4:].split()
        
        # convert each int string into int
        # then hex two-char ascii
        tokenString = ""
        for code in bytes:
            tokenString = tokenString + "{0:02x}".format(int(code))
            
        publish.single(MQTT_TOPIC, payload=tokenString, qos=0, retain=False, hostname=MQTT_HOST,
            port=MQTT_PORT, client_id="", keepalive=60, will=None, auth=None, tls=None,
            protocol=mqtt.MQTTv311)

#create an INET, STREAMing socket
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error:
    print('Failed to create socket')
    sys.exit()

# outer loop to maintain tcp connection
while True:

    try:
        # connect to tcp server
        s.connect((REMOTE_ADDR , REMOTE_PORT))
        # sock.setblocking(False)
    
        try:
            rcvd = b''
            # inner loop reading data from socket
            while True:
                rcvd = rcvd + s.recv(TCP_BUFFER_SIZE)
                
                if str(LINE_DELIMITER).encode('utf-8') in rcvd:
                    
                    delimiters = rcvd.count(str(LINE_DELIMITER).encode('utf-8'))
                    lines = rcvd.splitlines()

                    rcvd = b'' # clear receive buffer

                    # throw data back into the buffer if it appears incomplete
                    # need to test this in production
                    if (len(lines) != delimiters):
                        rcvd = lines[len(lines)-1]

                    for line in lines:
                        try:
                            processReceivedLine(line)
                        except:
                            print("unable to process line: " + str(line))

                # time.sleep(0.1) # arbitrary 10 Hz read refresh
                    
        except socket.timeout:
            print ("socket timeout, reconnecting")

    except ConnectionRefusedError:
        print ("connection refused, retrying")
        time.sleep(1) # delay reconnect attempt for 1 second


# close the socket - its true, we will never get here
#s.close()



